from rest_framework import generics, permissions
from django.contrib.auth import get_user_model
from .models import Todo
from .serializers import TodoSerializer, UserSerializer
from .permissions import IsAuthorOrReadOnly

# Create your views here.
class ListTodo(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    
class DetailTodo(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    
    
class UserList(generics.ListCreateAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    
    
class UserDetail(generics.ListCreateAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    
    